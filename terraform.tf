terraform {
  backend "s3" {
    region  = "us-east-2"
    key     = "habi/serverless.tfstate"
    encrypt = false
    bucket  = "infra-state-test"
  }
}
