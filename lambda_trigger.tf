# Api Gateway
resource "aws_lambda_permission" "api_invoke_signed_urls_lambda_permission" {
  statement_id  = "AllowExecutionFromApiGW"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.upload_images_lambda.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_api_gateway_rest_api.main_rest_api.execution_arn}/*/POST/upload-images"
}
