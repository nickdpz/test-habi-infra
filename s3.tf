# resource "aws_s3_bucket" "infra_state_bucket" {
#   acl           = "private"
#   provider      = aws.s3
#   bucket        = "infra-state-test"
#   force_destroy = false
#   versioning {
#     enabled = false
#   }
# }

# resource "aws_s3_bucket_object" "stages_bucket_objects" {
#   key     = "habi"
#   bucket  = aws_s3_bucket.infra_state_bucket.id
#   content = "dummy"

#   tags = {
#     origin      = "terraform",
#     environment = "all",
#     stack_id    = "test-all"
#   }

#   depends_on = [aws_s3_bucket.infra_state_bucket]
# }

resource "aws_s3_bucket" "images_bucket" {
  bucket   = "${var.stack_id}-images-bucket"
  acl      = "private"
  provider = aws.s3

  versioning {
    enabled = false
  }

  tags = merge({ "Name" : "${var.stack_id}-images-bucket" }, local.common_tags)
}