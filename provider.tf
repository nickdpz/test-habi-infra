provider "aws" {
  region = local.aws_region
}
provider "aws" {
  alias  = "s3"
  region = local.aws_region
}
