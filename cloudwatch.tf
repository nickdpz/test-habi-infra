resource "aws_cloudwatch_log_group" "upload_images_lambda_log_group" {
  name              = "/aws/lambda/${var.stack_id}-upload-images"
  retention_in_days = 1
}
