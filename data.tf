# Default
data "aws_iam_policy_document" "lambda_assume_role_policy_document" {
  statement {
    sid     = "LambdaRoleAssume"
    effect  = "Allow"
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

# Custom 
data "aws_iam_policy_document" "logging_policy_document" {
  statement {
    sid    = "LogginCW"
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = ["arn:aws:logs:*:*:*"]
  }
}

# Lambdas

# 1
data "aws_iam_policy_document" "upload_images_lambda_policy_document" {
  statement {
    sid     = "ResizesImages"
    effect  = "Allow"
    actions = ["s3:getObject", "s3:putObject", "s3:putObjectAcl"]
    resources = [
      aws_s3_bucket.images_bucket.arn,
      "${aws_s3_bucket.images_bucket.arn}/*"
    ]
  }
}