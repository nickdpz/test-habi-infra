# CloudWatch

resource "aws_iam_policy" "logging_policy" {
  name        = "${var.stack_id}-logging-policy"
  description = "Log Policy"
  path        = "/"
  policy      = data.aws_iam_policy_document.logging_policy_document.json
}

# Lambdas

# 1

resource "aws_iam_role" "upload_images_lambda_role" {
  name               = "${var.stack_id}-upload-images-lambda-role"
  assume_role_policy = data.aws_iam_policy_document.lambda_assume_role_policy_document.json
}

resource "aws_iam_policy" "upload_images_lambda_policy" {
  name        = "${var.stack_id}-resizes-upload-images-policy"
  description = "Custom policy lambda resizes upload images"
  policy      = data.aws_iam_policy_document.upload_images_lambda_policy_document.json
}

resource "aws_iam_role_policy_attachment" "upload_images_lambda_policy_attachment" {
  role       = aws_iam_role.upload_images_lambda_role.name
  policy_arn = aws_iam_policy.upload_images_lambda_policy.arn
}

resource "aws_iam_role_policy_attachment" "upload_images_lambda_logs_policy_attachment" {
  role       = aws_iam_role.upload_images_lambda_role.name
  policy_arn = aws_iam_policy.logging_policy.arn
}