locals {
  aws_region = "us-east-2"
  common_tags = {
    stack_id    = var.stack_id,
    environment = var.environment,
    origin      = "terraform"
  }
  api_gateway_body = file("./files/api-docs.json")
  lamba_body       = filebase64sha256("./files/main.zip")
  lambda_filename  = "./files/main.zip"
}