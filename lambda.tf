resource "aws_lambda_function" "upload_images_lambda" {
  function_name    = "${var.stack_id}-upload-image"
  role             = aws_iam_role.upload_images_lambda_role.arn
  handler          = "src/api.lambda_handler"
  runtime          = "python3.9"
  source_code_hash = local.lamba_body
  filename         = local.lambda_filename
  tags             = local.common_tags
  environment {
    variables = {
      "NODE_ENV" = "${var.environment}"
    }
  }

  depends_on = [
    aws_iam_role_policy_attachment.upload_images_lambda_policy_attachment,
    aws_cloudwatch_log_group.upload_images_lambda_log_group,
  ]

  lifecycle {
    ignore_changes = [
      filename,
      source_code_hash
    ]
  }
}
